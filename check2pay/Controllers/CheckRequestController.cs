﻿using Newtonsoft.Json;
using Business.Interfaces;
using DAL.Models;
using Microsoft.AspNetCore.Mvc;

namespace check2pay.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CheckRequestController : Controller
    {
        private readonly ITestProcessRepository _testServiceRepository;
        private readonly IServiceRepository _serviceRepository;
        public CheckRequestController(ITestProcessRepository testServiceRepository, IServiceRepository serviceRepository)
        {
            _testServiceRepository = testServiceRepository;
            _serviceRepository = serviceRepository;
        }

        //[ApiExplorerSettings(IgnoreApi = true)]
        ////[HttpGet(Name = "GetTestProcess")] 
        //public Task<IEnumerable<CheckRequestHistory>> GetData(string PRV_ID)
        //{
        //    LOGGER.LoggerExecute("Started... ", this);
        //    var result = _testServiceRepository.Check(PRV_ID, this);

        //    string res = JsonConvert.SerializeObject(result.Result);
        //    LOGGER.LoggerExecute("TestProcessData... " + res, this);
        //    return result;
        //}
        //
        [Route("[action]")]
        [HttpGet]
        public Task<IEnumerable<CheckRequestHistory>> MultiCheck(string PRV_ID)
        {
            LOGGER.LoggerExecute("Started GetMulti... ", this);

            var result = _testServiceRepository.Check(PRV_ID, this);
            string res = JsonConvert.SerializeObject(result.Result);

            LOGGER.LoggerExecute("TestProcessData... " + res, this);
            return result;
        }
        [Route("[action]")]
        [HttpGet]
        public async Task<CheckRequestHistory> SingleCheck(int PRV_ID)
        {
            LOGGER.LoggerExecute("Started GetMulti... ", this);

            var result =await _testServiceRepository.SingleCheck(PRV_ID, this);
            string res = JsonConvert.SerializeObject(result);

            LOGGER.LoggerExecute("TestProcessData... " + res, this);
            return result;
        }


        [Route("[action]")]
        [HttpGet]
        public List<CheckHistory> CheckRequestHistoriesMulti(int? serviceId,int? partitionNum, string? srv_name, int? status,DateTime from_date, DateTime to_date)
        {
            return _serviceRepository.CheckHistory(serviceId, partitionNum, srv_name,status, from_date,  to_date);
        }


        [Route("[action]")]
        [HttpGet]
        public List<CheckHistory> CheckRequestHistories()
        {
            return _serviceRepository.CheckHistory();
        }


        [Route("[action]/{serviceId}")]
        [HttpGet]
        public List<CheckHistory> CheckRequestHistories(int serviceId)
        {
            return _serviceRepository.CheckHistory(serviceId);
        }
        //{fromDate?}/{toDate?}
        [Route("[action]")]
        [HttpGet]
        public string ExportHistory(int? srv_id, string? srv_name, int? part_num)
        {
            return _serviceRepository.ExportHistory(srv_id,part_num, srv_name);
        }

        [Route("CheckAllServices")]
        [HttpGet]
        public Task<IEnumerable<CheckRequestHistory>> CheckAllServices()
        {
            LOGGER.LoggerExecute("Started CheckAllServices... ", this);

            var result = _testServiceRepository.CheckAll(this);
            string res = JsonConvert.SerializeObject(result.Result);

            LOGGER.LoggerExecute("TestProcessData... " + res, this);
            return result;
        }

        [Route("CheckAllServicesTop")]
        [HttpGet]
        public Task<IEnumerable<CheckRequestHistory>> CheckTop()
        {
            LOGGER.LoggerExecute("Started CheckAllServices... ", this);

            var result = _testServiceRepository.CheckTop(this);
            string res = JsonConvert.SerializeObject(result.Result);

            LOGGER.LoggerExecute("TestProcessData... " + res, this);
            return result;
        }

        [Route("CheckAllServicesAutomation")]
        [HttpGet]
        public string CheckAllAutomation()
        {
            LOGGER.LoggerExecute("Started CheckAllServices... ", this);

            string result = _testServiceRepository.CheckAllAutomation(this);
            //string res = JsonConvert.SerializeObject(result.Result);

            LOGGER.LoggerExecute("TestProcessData... " + result, this);
            return result;
        }
    }
}