﻿using Microsoft.AspNetCore.Mvc;
using Business.Interfaces;
using DAL.Models;
using DAL.ViewModels;



using HttpDeleteAttribute = Microsoft.AspNetCore.Mvc.HttpDeleteAttribute;
using HttpGetAttribute = Microsoft.AspNetCore.Mvc.HttpGetAttribute;
using HttpPostAttribute = Microsoft.AspNetCore.Mvc.HttpPostAttribute;
using RouteAttribute = Microsoft.AspNetCore.Mvc.RouteAttribute;
using Microsoft.AspNetCore.Authorization;

namespace check2pay.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ServicesController : ControllerBase
    {
        private readonly IServiceRepository _serviceRepository;
        public ServicesController(IServiceRepository serviceRepository)
        {
            _serviceRepository = serviceRepository;
        }


        [Route("[action]")]
        [HttpGet]
        public async Task<ServiceVM> GetServices([FromQuery] string[] ids, int groupId, int limit, int offset)
        {
            ServiceVM serviceVM = await _serviceRepository.Get(ids, groupId, limit, offset);
            return serviceVM;
        }
        [Route("[action]")]
        [HttpGet]
        public async Task<List<SimplifiedServiceVM>> GetAllSimplifiedServices()
        {
            List<SimplifiedServiceVM> services = await _serviceRepository.GetAllSimplifiedServices();
            return services;
        }
        //[Route("[action]")]
        //[HttpGet]
        //public async Task<ServiceVM> FilterServiceById([FromQuery] string[] ids)
        //{
        //    List<service> services = await _serviceRepository.FilterService(ids);

        //    ServiceVM serviceVM = new ServiceVM();
        //    serviceVM.Services = services;
        //    serviceVM.TotalCount = services.Count;

        //    return serviceVM;
        //}


        //[Route("[action]")]
        //[HttpGet]
        //public async Task<ServiceVM> FilterService([FromQuery] string[] ids, int groupId)
        //{
        //    List<service> services = await _serviceRepository.FilterService(ids, groupId);

        //    ServiceVM serviceVM = new ServiceVM();
        //    serviceVM.Services = services;
        //    serviceVM.TotalCount = services.Count;

        //    return serviceVM;
        //}

        [Route("[action]")]
        [HttpGet]
        public async Task<List<Group>> GetGroups()
        {
            List<Group> groups = await _serviceRepository.GetGroups();
            return groups;
        }
        [Route("[action]")]
        [HttpPost]
        public async Task<bool> UpdateParameter(List<CheckRequestParameters> prms)
        {
            return await _serviceRepository.UpdateParametersBySrvID(prms);
        }

        [Route("[action]")]
        [HttpPost]
        public async Task<bool> AddParameter(CheckRequestParameters prm)
        {
            return await _serviceRepository.AddParameterToService(prm);
        }


        [Route("[action]/{id}")]
        [HttpDelete]
        public bool RemoveParameter(int id)
        {
            return _serviceRepository.RemoveParameter(id);
        }
        [Route("[action]")]
        [HttpPost]
        public string LastSuccesfulParameters()
        {
            return _serviceRepository.LastSuccesfulParameters();
        }
        [Route("[action]/{serviceId}")]
        [HttpGet]
        public async Task<IEnumerable<CheckRequestParameters>> GetParametersList(int serviceId)
        {
            return await _serviceRepository.GetParametersList(serviceId);
        }


        [Route("[action]")]
        [HttpGet]
        public string ExportHistory(int? srv_id, string? srv_name, int? part_num)
        {
            return _serviceRepository.ExportHistory(srv_id, part_num, srv_name);
        }

    }
}