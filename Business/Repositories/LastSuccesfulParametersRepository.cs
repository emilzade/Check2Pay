﻿using Business.Interfaces;
using DAL.Models;

namespace Business.Repositories
{
    public class LastSuccesfulParametersRepository : ILastSuccesfulParameters
    {
        private readonly PSA_Context _pSA_Context;
        private readonly Main_Context _main_Context;

        public LastSuccesfulParametersRepository(PSA_Context context, Main_Context mainContext)
        {
            _pSA_Context = context;
            _main_Context = mainContext;
        }

        public Task<IEnumerable<LastSuccesfulParameters>> GetLastSuccesfulParameters()
        {
            throw new NotImplementedException();
        }
    }
}