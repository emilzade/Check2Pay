﻿using Dapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DAL.Classes;
using Business.Interfaces;
using DAL.Models;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq.Expressions;
using System.Net;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory.Database;
using static DAL.Models.GateResponseModel;

namespace Business.Repositories
{
    public class TestProcessRepository : ITestProcessRepository
    {
        private readonly PSA_Context _pSA_Context;
        ExcellWorks excellWorks = new ExcellWorks();
        private const string UPG_address = "http://10.203.180.8:8080/XmlGate/OsmpGate.aspx?agt_id=17&command=check&online_trm_prv_id=1308319525&sum=1.00&sum_from=1.00&trm_id=4244&txn_id=";  //1021486693 0000004702";
        public TestProcessRepository(PSA_Context context)
        {
            _pSA_Context = context;
        }
        public async Task<IEnumerable<CheckRequestParameters>> GetServiceTestDataList(string prv_id)
        {
            return await _pSA_Context.checkrequestparameters.ToListAsync();
        }
        string CheckRequestToGATE(string requestString, ControllerBase cntrl, ref CheckRequestHistory item)
        {
            string response = String.Empty;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(requestString);
            //LOGGER.LoggerExecute("RequestString = " + requestString, cntrl);
            request.Method = "GET";
            request.ContentType = "application/x-www-form-urlencoded";

            item.start_time = DateTime.Now;
            var webResponse = request.GetResponse();
            var webStream = webResponse.GetResponseStream();
            var responseReader = new StreamReader(webStream);
            response = responseReader.ReadToEnd();
            //LOGGER.LoggerExecute("Response = " + response, cntrl);
            responseReader.Close();
            GateResponse gateResponse = SerializerWithoutNamespace<GateResponse>.Deserialize(response);
            item.end_time = DateTime.Now;
            item.resultcodeid = Convert.ToInt32(gateResponse.Result);
            item.exceptiondata = gateResponse.Comment;
            LOGGER.LoggerExecute("Response RESULT = " + gateResponse.Result.ToString(), cntrl);
            LOGGER.LoggerExecute("Response Comment = " + gateResponse.Result.ToString(), cntrl);
            return response;
        }
        public async Task<IEnumerable<CheckRequestHistory>> CheckRequestPartition(int partitionnumber)
        {
            if (partitionnumber == -1)
            {
                return await _pSA_Context.checkrequesthistory.ToListAsync();
            }
            else
            {
                return await _pSA_Context.checkrequesthistory.Where(p => p.partitionnumber == partitionnumber).ToListAsync();
            }
        }
        int GetMaxPartitionNumber()
        {
            int maxValue;
            if (_pSA_Context.checkrequesthistory.Count() == 0)
            {

                maxValue = 1;
            }

            else
            {
                maxValue = _pSA_Context.checkrequesthistory.Max(p => p.partitionnumber);
            }
            return maxValue;
        }
        int GetMaxTransactionNumber()
        {
            var maxValue = (from s in _pSA_Context.settings
                            where s.id == 1
                            select s.value).SingleOrDefault();

            return Convert.ToInt32(maxValue);
        }
        public void UpdateSettings()
        {
            var test = _pSA_Context.settings.First(s => s.id == 1);
            test.value = test.value + 1;
            _pSA_Context.SaveChanges();
        }
        public async Task<IEnumerable<CheckRequestHistory>> Check(string prv_id_list, ControllerBase cntrl)
        {
            LOGGER.LoggerExecute("Check started... ", cntrl);
            CheckRequestHistory checkProcessItem;
            int newPartitionNumber = GetMaxPartitionNumber() + 1;
            // string newTransactionNumber = GetMaxTransactionNumber() + 1 + "0000004702";
            LOGGER.LoggerExecute("new PartitionNumber = " + newPartitionNumber, cntrl);
            string[] idlist = prv_id_list.Split(',');
            List<service> serviceData;
            List<CheckRequestParameters> serviceJOINEDData;

            foreach (string id in idlist)
            {
                int a = Convert.ToInt32(id);
                checkProcessItem = new CheckRequestHistory();
                checkProcessItem.partitionnumber = newPartitionNumber;
                serviceData = _pSA_Context.service.Where(s => s.gate_service_id == a).ToList();
                checkProcessItem.serviceid = serviceData[0].gate_service_id;
                serviceJOINEDData = _pSA_Context.checkrequestparameters.Where(s => s.serviceid == serviceData[0].gate_service_id).ToList();
                LOGGER.LoggerExecute("Check serviceData = " + serviceData.Count, cntrl);
                string paramString = String.Empty;
                foreach (var sjd in serviceJOINEDData)
                {
                    paramString += String.Format("&{0}={1}", sjd.name, sjd.value);
                }
                string newTransactionNumber = GetMaxTransactionNumber() + "0000004702";
                UpdateSettings();
                string address = UPG_address + newTransactionNumber + paramString + "&prv_id=" + id;
                checkProcessItem.request = address;
                LOGGER.LoggerExecute("REQUEST to UPG GATE = " + address, cntrl);
                string response = CheckRequestToGATE(address, cntrl, ref checkProcessItem);
                checkProcessItem.response = response;

                LOGGER.LoggerExecute("RESPONSE from UPG GATE = " + response, cntrl);
                LOGGER.LoggerExecute("checkProcessItem = " + checkProcessItem.ToString(), cntrl);
                UpdateSettings();
                _pSA_Context.checkrequesthistory.Add(checkProcessItem);
                _pSA_Context.SaveChanges();
            }
            return await _pSA_Context.checkrequesthistory.Where(p => p.partitionnumber == newPartitionNumber).ToListAsync();
        }

        public async Task<CheckRequestHistory> SingleCheck(int serviceId, ControllerBase cntrl)
        {
            LOGGER.LoggerExecute("Check started... ", cntrl);
            CheckRequestHistory checkProcessItem;
            int newPartitionNumber = GetMaxPartitionNumber() + 1;
            // string newTransactionNumber = GetMaxTransactionNumber() + 1 + "0000004702";
            LOGGER.LoggerExecute("new PartitionNumber = " + newPartitionNumber, cntrl);
            service serviceData;
            List<CheckRequestParameters> serviceJOINEDData;


            int a = serviceId;
            checkProcessItem = new CheckRequestHistory();
            checkProcessItem.partitionnumber = newPartitionNumber;
            serviceData = await _pSA_Context.service.Where(s => s.gate_service_id == a).SingleOrDefaultAsync();
            if (serviceData is null)
            {
                throw new NullReferenceException();
            }
            checkProcessItem.serviceid = serviceData.gate_service_id;
            serviceJOINEDData = await _pSA_Context.checkrequestparameters.Where(s => s.serviceid == serviceData.gate_service_id).ToListAsync();
            LOGGER.LoggerExecute("Check serviceData = " + serviceData, cntrl);
            string paramString = String.Empty;
            foreach (var sjd in serviceJOINEDData)
            {
                paramString += String.Format("&{0}={1}", sjd.name, sjd.value);
            }
            string newTransactionNumber = GetMaxTransactionNumber() + "0000004702";
            UpdateSettings();
            string address = UPG_address + newTransactionNumber + paramString + "&prv_id=" + serviceId;
            checkProcessItem.request = address;
            LOGGER.LoggerExecute("REQUEST to UPG GATE = " + address, cntrl);
            string response = CheckRequestToGATE(address, cntrl, ref checkProcessItem);
            checkProcessItem.response = response;

            LOGGER.LoggerExecute("RESPONSE from UPG GATE = " + response, cntrl);
            LOGGER.LoggerExecute("checkProcessItem = " + checkProcessItem.ToString(), cntrl);
            UpdateSettings();
            await _pSA_Context.checkrequesthistory.AddAsync(checkProcessItem);
            await _pSA_Context.SaveChangesAsync();

            return await _pSA_Context.checkrequesthistory.Where(p => p.partitionnumber == newPartitionNumber).FirstOrDefaultAsync();
        }
        public async Task<IEnumerable<CheckRequestHistory>> CheckAll(ControllerBase cntrl)
        {
            string str = "";
            using (var con = _pSA_Context.Database.GetDbConnection())
            {
                List<int> lst1 = new List<int>();
                lst1 = con.Query<int>("SELECT distinct p.serviceid FROM check2pay.checkrequestparameters p inner join check2pay.service s on p.serviceid =s.gate_service_id ").ToList();
                foreach (var item in lst1)
                {
                    str = str + item.ToString() + ",";
                }

                string prv_id_list = str.Remove(str.Length - 1);
                LOGGER.LoggerExecute("CheckAll started... ", cntrl);
                CheckRequestHistory checkProcessItem;
                int newPartitionNumber = GetMaxPartitionNumber() + 1;

                LOGGER.LoggerExecute("new PartitionNumber = " + newPartitionNumber, cntrl);
                string[] idlist = prv_id_list.Split(',');
                List<service> serviceData = new List<service>();
                List<CheckRequestParameters> serviceJOINEDData;
                foreach (string id in idlist)
                {
                    int a = Convert.ToInt32(id);
                    checkProcessItem = new CheckRequestHistory();
                    checkProcessItem.partitionnumber = newPartitionNumber;
                    serviceData = _pSA_Context.service.Where(s => s.gate_service_id == a).ToList();
                    checkProcessItem.serviceid = serviceData[0].gate_service_id;
                    serviceJOINEDData = _pSA_Context.checkrequestparameters.Where(s => s.serviceid == serviceData[0].gate_service_id).ToList();
                    LOGGER.LoggerExecute("Check serviceData = " + serviceData.Count, cntrl);

                    string paramString = String.Empty;
                    foreach (var sjd in serviceJOINEDData)
                    {
                        paramString += String.Format("&{0}={1}", sjd.name, sjd.value);
                    }
                    string newTransactionNumber = GetMaxTransactionNumber() + "0000004244";
                    UpdateSettings();
                    string address = UPG_address + newTransactionNumber + paramString + "&prv_id=" + id;
                    checkProcessItem.request = address;
                    LOGGER.LoggerExecute("REQUEST to UPG GATE = " + address, cntrl);
                    string response = CheckRequestToGATE(address, cntrl, ref checkProcessItem);
                    checkProcessItem.response = response;
                    LOGGER.LoggerExecute("RESPONSE from UPG GATE = " + response, cntrl);
                    LOGGER.LoggerExecute("checkProcessItem = " + checkProcessItem.ToString(), cntrl);

                    _pSA_Context.checkrequesthistory.Add(checkProcessItem);
                    _pSA_Context.SaveChanges();
                }

                return await _pSA_Context.checkrequesthistory.Where(p => p.partitionnumber == newPartitionNumber).ToListAsync();
            }
        }
        public string CheckAllAutomation(ControllerBase cntrl)
        {
            string str = "";
            //using (var con = _pSA_Context.Database.GetDbConnection())
            //{
            try
            {
                var con = _pSA_Context.Database.GetDbConnection();
                List<int> lst1 = new List<int>();
                //lst1 = con.Query<int>("SELECT distinct p.serviceid FROM check2pay.checkrequestparameters p inner join check2pay.service s on p.serviceid =s.gate_service_id ").ToList();
                lst1 = con.Query<int>("SELECT distinct p.serviceid FROM check2pay.checkrequestparameters p inner join check2pay.service s on p.serviceid =s.gate_service_id limit 4").ToList();
                foreach (var item in lst1)
                {
                    str = str + item.ToString() + ",";
                }

                string prv_id_list = str.Remove(str.Length - 1);
                LOGGER.LoggerExecute("CheckAll started... ", cntrl);
                CheckRequestHistory checkProcessItem;
                int newPartitionNumber = GetMaxPartitionNumber() + 1;

                LOGGER.LoggerExecute("new PartitionNumber = " + newPartitionNumber, cntrl);
                string[] idlist = prv_id_list.Split(',');
                List<service> serviceData = new List<service>();
                List<CheckRequestParameters> serviceJOINEDData;
                foreach (string id in idlist)
                {
                    int a = Convert.ToInt32(id);
                    checkProcessItem = new CheckRequestHistory();
                    checkProcessItem.partitionnumber = newPartitionNumber;
                    serviceData = _pSA_Context.service.Where(s => s.gate_service_id == a).ToList();
                    checkProcessItem.serviceid = serviceData[0].gate_service_id;
                    serviceJOINEDData = _pSA_Context.checkrequestparameters.Where(s => s.serviceid == serviceData[0].gate_service_id).ToList();
                    LOGGER.LoggerExecute("Check serviceData = " + serviceData.Count, cntrl);

                    string paramString = String.Empty;
                    foreach (var sjd in serviceJOINEDData)
                    {
                        paramString += String.Format("&{0}={1}", sjd.name, sjd.value);
                    }
                    string newTransactionNumber = GetMaxTransactionNumber() + "0000004244";
                    UpdateSettings();
                    string address = UPG_address + newTransactionNumber + paramString + "&prv_id=" + id;
                    checkProcessItem.request = address;
                    LOGGER.LoggerExecute("REQUEST to UPG GATE = " + address, cntrl);
                    string response = CheckRequestToGATE(address, cntrl, ref checkProcessItem);
                    checkProcessItem.response = response;
                    LOGGER.LoggerExecute("RESPONSE from UPG GATE = " + response, cntrl);
                    LOGGER.LoggerExecute("checkProcessItem = " + checkProcessItem.ToString(), cntrl);

                    _pSA_Context.checkrequesthistory.Add(checkProcessItem);
                    _pSA_Context.SaveChanges();
                }
                ExcellWorks excellWorks = new ExcellWorks();
                string filename = excellWorks.CreateAndImport(null, null, newPartitionNumber, _pSA_Context);
                //  return await _pSA_Context.checkrequesthistory.Where(p => p.partitionnumber == newPartitionNumber).ToListAsync();
                return filename;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public async Task<IEnumerable<CheckRequestHistory>> CheckTop(ControllerBase cntrl)
        {

            try
            {
                int newPartitionNumber;
                string str = "";
                // Task<List<CheckRequestHistory>> datas;
                //using (var con = _pSA_Context.Database.GetDbConnection())
                //{
                var con = _pSA_Context.Database.GetDbConnection();
                List<int> lst1 = new List<int>();
                lst1 = con.Query<int>("SELECT serviceid FROM check2pay.groupservices").ToList();
                foreach (var item in lst1)
                {
                    str = str + item.ToString() + ",";
                }

                string prv_id_list = str.Remove(str.Length - 1);
                LOGGER.LoggerExecute("CheckAll started... ", cntrl);
                CheckRequestHistory checkProcessItem;
                newPartitionNumber = GetMaxPartitionNumber() + 1;

                LOGGER.LoggerExecute("new PartitionNumber = " + newPartitionNumber, cntrl);
                string[] idlist = prv_id_list.Split(',');
                List<service> serviceData = new List<service>();
                List<CheckRequestParameters> serviceJOINEDData;
                foreach (string id in idlist)
                {
                    int a = Convert.ToInt32(id);
                    checkProcessItem = new CheckRequestHistory();
                    checkProcessItem.partitionnumber = newPartitionNumber;
                    serviceData = _pSA_Context.service.Where(s => s.gate_service_id == a).ToList();
                    checkProcessItem.serviceid = serviceData[0].gate_service_id;
                    serviceJOINEDData = _pSA_Context.checkrequestparameters.Where(s => s.serviceid == serviceData[0].gate_service_id).ToList();
                    LOGGER.LoggerExecute("Check serviceData = " + serviceData.Count, cntrl);

                    string paramString = String.Empty;
                    foreach (var sjd in serviceJOINEDData)
                    {
                        paramString += String.Format("&{0}={1}", sjd.name, sjd.value);
                    }
                    string newTransactionNumber = GetMaxTransactionNumber() + "0000004244";
                    UpdateSettings();
                    string address = UPG_address + newTransactionNumber + paramString + "&prv_id=" + id;
                    checkProcessItem.request = address;
                    LOGGER.LoggerExecute("REQUEST to UPG GATE = " + address, cntrl);
                    string response = CheckRequestToGATE(address, cntrl, ref checkProcessItem);
                    checkProcessItem.response = response;
                    LOGGER.LoggerExecute("RESPONSE from UPG GATE = " + response, cntrl);
                    LOGGER.LoggerExecute("checkProcessItem = " + checkProcessItem.ToString(), cntrl);

                    await _pSA_Context.checkrequesthistory.AddAsync(checkProcessItem);
                    await _pSA_Context.SaveChangesAsync();
                }
                ExcellWorks excellWorks = new ExcellWorks();
                excellWorks.CreateAndImport(null, null, newPartitionNumber, _pSA_Context);
                // }
                //using (var con = _pSA_Context.Database.GetDbConnection())
                //{

                //}
                //ExcellWorks excellWorks= new ExcellWorks();
                //excellWorks.CreateAndImport(null, null, newPartitionNumber, _pSA_Context);
                return await _pSA_Context.checkrequesthistory.Where(p => p.partitionnumber == newPartitionNumber).ToListAsync(); ;

            }

            catch (Exception ex)
            {

                throw;
            }
        }
    }
}