﻿using Microsoft.EntityFrameworkCore;
using DAL.Models;
using Dapper;
using DAL.Classes;
using Business.Interfaces;
using DAL.ViewModels;

namespace Business.Repositories
{
    public class ServiceRepository : IServiceRepository
    {
        private readonly PSA_Context _pSA_Context;
        private readonly Main_Context _main_Context;
        public ServiceRepository(PSA_Context context, Main_Context mainContext)
        {
            _pSA_Context = context;
            _main_Context = mainContext;
        }
        public async Task<ServiceVM> Get(string[] ids, int groupId, int limit, int offset)
        {
            ServiceVM serviceVM = new ServiceVM();
            List<service> services = new List<service>();
            int totalCount = 0;

            if (ids.Length > 0)
            {
                for (int i = 0; i < ids.Length; i++)
                {
                    int newId = Convert.ToInt32(ids[i]);
                    List<service> foundServices = new List<service>();
                    foundServices = await _pSA_Context.service.OrderBy(x => x.topservicesid).Where(x => x.gate_service_id.ToString().Contains(ids[i])).Where(x => x.group_id == groupId || groupId == 0).Skip(offset).Take(limit).ToListAsync();
                    totalCount += await _pSA_Context.service.Where(x => x.gate_service_id.ToString().Contains(ids[i])).Where(x => x.group_id == groupId || groupId == 0).CountAsync();

                    for (int j = 0; j < foundServices.Count; j++)
                    {
                        if (!services.Any(x => x.id == foundServices[j].id)) services.Add(foundServices[j]);
                    }
                }
            }
            else
            {
                services = await _pSA_Context.service.OrderBy(x => x.topservicesid).Where(x => x.group_id == groupId || groupId == 0).Skip(offset).Take(limit).ToListAsync();
                totalCount = await _pSA_Context.service.Where(x => x.group_id == groupId || groupId == 0).CountAsync();
            }

            //}
            serviceVM.Services = services;
            serviceVM.TotalCount = totalCount;
            return serviceVM;

            //var con = _pSA_Context.Database.GetDbConnection();
            //string sql;
            //DynamicParameters parameters = new DynamicParameters();
            //if (name is null)
            //{
            //    sql = @"select gs.separate ,gs.name, ch.* from check2pay.checkrequesthistory as ch 
            //               left join check2pay.service as gs on ch.serviceid = gs.gate_service_id
            //               where ch.resultcodeid=@status and ch.serviceid = coalesce(@s_id,ch.serviceid) and ch.start_time between @startdate and @enddate and partitionnumber = coalesce(@prt_num,partitionnumber) order by ch.start_time desc;";

            //    parameters.Add("prt_num", partition_num);
            //    parameters.Add("s_id", serviceId);
            //    parameters.Add("startdate", from_date);
            //    parameters.Add("enddate", to_date);
            //    parameters.Add("status", status);
            //}
            //else
            //{
            //    sql = @"select gs.separate ,gs.name, ch.* from check2pay.checkrequesthistory as ch 
            //               left join check2pay.service as gs on ch.serviceid = gs.gate_service_id
            //                 where ch.resultcodeid=@status and ch.serviceid = coalesce(@s_id,ch.serviceid) and gs.name ILIKE '%" + srv_name + "%' and ch.start_time between @startdate and @enddate and " +
            //               "partitionnumber = coalesce(@prt_num,partitionnumber) order by ch.start_time desc";
            //    parameters.Add("s_id", serviceId);
            //    parameters.Add("srv_nam", srv_name);
            //    parameters.Add("prt_num", partition_num);
            //    parameters.Add("startdate", from_date);
            //    parameters.Add("enddate", to_date);
            //    parameters.Add("status", status);

            //}
            //list = con.Query<CheckHistory>(sql, parameters).ToList();
            //return list;



        }
        public async Task<List<SimplifiedServiceVM>> GetAllSimplifiedServices()
        {
            List<SimplifiedServiceVM> services = await _pSA_Context.service.Select(x => new SimplifiedServiceVM { gate_service_id = x.gate_service_id, name = x.name }).ToListAsync();
            return services;
        }
        public async Task<int> GetTotalServiceCount()
        {
            int totalCount = await _pSA_Context.service.CountAsync();
            return totalCount;
        }
        public async Task<List<Group>> GetGroups()
        {
            DbWorks dbWorks = new DbWorks(_pSA_Context);
            List<Group> groups = dbWorks.GetGroups();
            return await _pSA_Context.groupservices.ToListAsync();
        }
        public async Task<IEnumerable<CheckRequestParameters>> GetServiceTestData(int prv_id)
        {
            try
            {
                service? service = _pSA_Context.service.Where(s => s.gate_service_id == prv_id).FirstOrDefault();
                if (service is null)
                {
                    throw new NullReferenceException(nameof(service));
                }
                int? serviceId = service.gate_service_id;

                return await _pSA_Context.checkrequestparameters.Where(t => t.serviceid == serviceId).ToListAsync();
            }
            catch (Exception ex)
            {
                throw new Exception(message: ex.Message);
            }
        }
        public CheckRequestParameters AddServiceTestData(ServiceTestDataView serviceTestDataview)
        {
            service? service = _pSA_Context.service.Where(s => s.gate_service_id == serviceTestDataview.prv_id).FirstOrDefault();
            if (service is null)
            {
                throw new NullReferenceException(nameof(service));
            }
            CheckRequestParameters? checkRequestParameters = new CheckRequestParameters(service.id, serviceTestDataview.paramname, serviceTestDataview.paramvalue);
            _pSA_Context.checkrequestparameters.Add(checkRequestParameters);
            _pSA_Context.SaveChanges();
            CheckRequestParameters? checkRequestParameter = _pSA_Context.checkrequestparameters.Where(t => t.id == checkRequestParameters.id).FirstOrDefault();
            if (checkRequestParameter is null)
            {
                throw new NullReferenceException(nameof(checkRequestParameter));
            }
            return checkRequestParameter;
        }
        public bool RemoveParameter(int id)
        {
            var data = _pSA_Context.checkrequestparameters.Where(t => t.id == id).FirstOrDefault();
            if (data is null)
            {
                throw new NullReferenceException(nameof(data));
            }
            else
            {
                try
                {
                    _pSA_Context.checkrequestparameters.Remove(data);
                    _pSA_Context.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
        }
        public string LastSuccesfulParameters()
        {
            using (var con = _pSA_Context.Database.GetDbConnection())
            {
                try
                {
                    con.Open();
                    con.Execute("call check2pay.parameters_procedur()");
                    return "succes";
                }
                catch (Exception)
                {
                    throw new Exception();
                }
            }
        }
        public async Task<IEnumerable<CheckRequestParameters>> GetParametersList(int? serviceId)
        {
            //  return await _pSA_Context.checkrequestparameters.ToListAsync();
            DbWorks db = new DbWorks(_pSA_Context);
            return await db.GetParametersByServiceId(serviceId);
        }
        List<CheckHistory> IServiceRepository.CheckHistory()
        {
            DbWorks db = new DbWorks(_pSA_Context);
            return db.GetHistory();
        }
        List<CheckHistory> IServiceRepository.CheckHistory(int? serviceId, int? partitionNum, string? srv_name, int? status, DateTime from_date, DateTime to_date)
        {
            DbWorks db = new DbWorks(_pSA_Context);
            return db.GetHistory(serviceId, srv_name, partitionNum, status, from_date, to_date);
        }
        List<CheckHistory> IServiceRepository.CheckHistory(int serviceId)
        {
            DbWorks db = new DbWorks(_pSA_Context);
            return db.GetHistory(serviceId);
        }
        List<CheckHistory> IServiceRepository.CheckHistory(DateTime fromDate, DateTime toDate)
        {
            DbWorks db = new DbWorks(_pSA_Context);
            return db.GetHistory(fromDate, toDate);
        }
        public string ExportHistory(int? serviceId, int? partitionNum, string? srv_name)
        {
            ExcellWorks exc = new ExcellWorks();
            //int? srv_id, string? srv_name, int? part_num, DateTime fromDate, DateTime toDate, PSA_Context pSA_Context
            return exc.CreateAndImport(serviceId, srv_name, partitionNum, _pSA_Context);
        }
        public Task<bool> UpdateParametersBySrvID(List<CheckRequestParameters> prms)
        {
            DbWorks db = new DbWorks(_pSA_Context);
            return db.UpdateParametersBySrvID(prms);
        }
        public Task<bool> AddParameterToService(CheckRequestParameters prm)
        {
            DbWorks db = new DbWorks(_pSA_Context);
            return db.AddParameterToSrv(prm);
        }
    }
}