﻿using DAL.Models;
using DAL.ViewModels;

namespace Business.Interfaces
{
    public interface IServiceRepository
    {
        Task<ServiceVM> Get(string[] id , int groupId, int limit, int offset);
        Task<List<SimplifiedServiceVM>> GetAllSimplifiedServices();
        Task<int> GetTotalServiceCount();
        Task<List<Group>> GetGroups();
        bool RemoveParameter(int id);
        string LastSuccesfulParameters();
        Task<IEnumerable<CheckRequestParameters>> GetParametersList(int? serviceId);
        List<CheckHistory> CheckHistory();
        List<CheckHistory> CheckHistory(int? serviceId, int? partitionNum, string? srv_name, int? status, DateTime from_date, DateTime to_date);
        List<CheckHistory> CheckHistory(int serviceId);
        List<CheckHistory> CheckHistory(DateTime fromDate, DateTime toDate);
        string ExportHistory(int? serviceId, int? partitionNum, string? srv_name);
        Task<bool> UpdateParametersBySrvID(List<CheckRequestParameters> prms);
        Task<bool> AddParameterToService(CheckRequestParameters prm);
    }
}