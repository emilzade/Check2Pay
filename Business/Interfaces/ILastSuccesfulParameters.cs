﻿using DAL.Models;

namespace Business.Interfaces
{
    public interface ILastSuccesfulParameters
    {
        public Task<IEnumerable<LastSuccesfulParameters>> GetLastSuccesfulParameters();
    }
}