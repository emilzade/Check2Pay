﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using DAL.Models;

namespace Business.Interfaces
{
    public interface ITestProcessRepository
    {
        public Task<IEnumerable<CheckRequestParameters>> GetServiceTestDataList(string prv_id);
        public Task<IEnumerable<CheckRequestHistory>> Check(string prv_id, ControllerBase cntrl);
        public Task<CheckRequestHistory> SingleCheck(int prv_id, ControllerBase cntrl);
        public Task<IEnumerable<CheckRequestHistory>> CheckAll(ControllerBase cntrl);
        public string CheckAllAutomation(ControllerBase cntrl);
        public Task<IEnumerable<CheckRequestHistory>> CheckTop(ControllerBase cntrl);
        public Task<IEnumerable<CheckRequestHistory>> CheckRequestPartition(int partitionnumber);
    }
}



