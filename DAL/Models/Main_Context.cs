﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace DAL.Models
{
    public class Main_Context:DbContext
    {
        protected readonly IConfiguration Configuration;
        public Main_Context(DbContextOptions<Main_Context> options) : base(options)
        {
            var constr = Database.GetConnectionString();
            var opt = options;
        }
        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            var constr = Configuration.GetConnectionString("Main");
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("dbo");
            base.OnModelCreating(modelBuilder);
            
        }
    }
}