﻿namespace DAL.Models
{
    public class Settings
    {
        public int id { get; set; }
        public string key { get; set; }
        public int value { get; set; }
    }
}
