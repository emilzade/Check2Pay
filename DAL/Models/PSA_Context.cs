﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
namespace DAL.Models
{
    public class PSA_Context : IdentityDbContext<AppUser>
    {
        protected readonly IConfiguration Configuration;
        public PSA_Context(DbContextOptions<PSA_Context> options) : base(options)
        {
            var constr = Database.GetConnectionString();
            var opt = options;
        }
        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            var constr = Configuration.GetConnectionString("check2pay");
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("check2pay");
            base.OnModelCreating(modelBuilder);

            // modelBuilder.Entity<ServiceTestData>()
            //.HasOne(st => st.Service)
            //.WithMany(s => s.TestDatas)  
            //.HasForeignKey(st => st.serviceid);
        }

        public DbSet<service> service { get; set; }
        public DbSet<CheckRequestParameters> checkrequestparameters { get; set; }
        public DbSet<CheckRequestHistory> checkrequesthistory { get; set; }
        public DbSet<Settings> settings { get; set; }
        public DbSet<LastSuccesfulParameters> lastSuccesfulParameters { get; set; }
        public DbSet<Group> groupservices { get; set; }
    }
}