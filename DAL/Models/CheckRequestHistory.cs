﻿using Newtonsoft.Json;

namespace DAL.Models
{
    public class CheckRequestHistory
    {
        public int? id { get; set; }
        public int? serviceid { get; set; }
        public DateTime? start_time { get; set; }
        public DateTime? end_time { get; set; }
        public int? userid { get; set; }
        public string? request { get; set; }
        public string? response { get; set; }
        public string? exceptiondata { get; set; }
        public int? resultcodeid { get; set; }
        public int partitionnumber { get; set; }
        public service service { get; set; }
        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}