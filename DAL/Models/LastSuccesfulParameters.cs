﻿namespace DAL.Models
{
    public class LastSuccesfulParameters
    {
        public int? id { get; set; }
        public int? serviceid { get; set; }
        public string? name { get; set; }
        public string? value { get; set; }
    }
}