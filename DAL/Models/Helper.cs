﻿using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace DAL.Models
{
    public class SerializerWithoutNamespace<T>
    {
        private static readonly XmlSerializerNamespaces _xmlns = new XmlSerializerNamespaces();

        private static readonly XmlSerializer _xs = new XmlSerializer(typeof(T));

        static SerializerWithoutNamespace()
        {
            _xmlns.Add("", "");
        }
        public static string Serialize(T obj, Encoding enc, bool formatXml = true, bool omitXmlDeclaration = false)
        {
            using (var ms = new MemoryStream())
            {
                //var xw = new XmlTextWriter(ms, enc);
                Serialize(ms, obj, formatXml, omitXmlDeclaration, enc);

                var xws = new XmlWriterSettings { };
                XmlWriter xw = XmlWriter.Create(ms, xws);
                byte[] bts = ms.ToArray();
                return enc.GetString(bts);
            }
        }
        public static string Serialize(T obj)
        {
            var strw = new StringWriter();
            _xs.Serialize(strw, obj, _xmlns);
            return strw.ToString();
        }
        public static void Serialize(XmlWriter xw, T obj)
        {
            _xs.Serialize(xw, obj, _xmlns);
        }
        public static void Serialize(TextWriter textWriter, T obj)
        {
            _xs.Serialize(textWriter, obj, _xmlns);
        }
        public static void Serialize(Stream ms, T obj)
        {
            _xs.Serialize(ms, obj, _xmlns);
        }
        public static void Serialize(Stream ms, T obj, bool formatXml = false, bool omitXmlDeclaration = false, Encoding enc = null)
        {
            var e = enc ?? Encoding.UTF8;
            var xws = new XmlWriterSettings { Encoding = e, OmitXmlDeclaration = omitXmlDeclaration };

            if (formatXml)
            {
                xws.NewLineChars = "\n";
                xws.NewLineHandling = NewLineHandling.Replace;
                xws.Indent = true;
            }
            XmlWriter xw = XmlWriter.Create(ms, xws);

            _xs.Serialize(xw, obj, _xmlns);
        }
        public static T Deserialize(XmlReader reader)
        {
            return (T)_xs.Deserialize(reader);
        }
        public static T Deserialize(string sXml)
        {
            using (var sr = new StringReader(sXml))
                return (T)_xs.Deserialize(sr);
        }
        public static T Deserialize(Stream stream)
        {
            return (T)_xs.Deserialize(stream);
        }
        public static T Deserialize(TextReader reader)
        {
            return (T)_xs.Deserialize(reader);
        }
    }
}