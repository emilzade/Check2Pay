﻿namespace DAL.Models
{
    public class CheckRequestParameters
    {
        public CheckRequestParameters()
        {

        }
        public CheckRequestParameters(int? _serviceId, string? _name, string? _value)
        {
            serviceid = _serviceId;
            name = _name;
            value = _value;
            active = true;
        }

        public int id { get; set; }
        public int? serviceid { get; set; }
        public string? name { get; set; }
        public string? value { get; set; }
        public bool? active { get; set; }
    }
    public class ServiceTestDataView
    {
        //public int id { get; set; }
        public int prv_id { get; set; }
        public string? paramname { get; set; }
        public string? paramvalue { get; set; }
        //public bool? active { get; set; }

        //public service? Service { get; set; }
    }
}
