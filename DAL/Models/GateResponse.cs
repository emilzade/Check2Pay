﻿using System.Xml.Serialization;

namespace DAL.Models
{
    public class GateResponseModel
    {
		[XmlRoot(ElementName = "field2")]
		public class Field2
		{
			[XmlAttribute(AttributeName = "name")]
			public string Name { get; set; }
			[XmlText]
			public string Text { get; set; }
		}

		[XmlRoot(ElementName = "field1")]
		public class Field1
		{
			[XmlAttribute(AttributeName = "name")]
			public string Name { get; set; }
			[XmlText]
			public string Text { get; set; }
		}

		[XmlRoot(ElementName = "fields")]
		public class Fields
		{
			[XmlElement(ElementName = "field2")]
			public Field2 Field2 { get; set; }
			[XmlElement(ElementName = "field1")]
			public Field1 Field1 { get; set; }
		}

		[XmlRoot(ElementName = "response")]
		public class GateResponse
		{
			[XmlElement(ElementName = "osmp_txn_id")]
			public string Osmp_txn_id { get; set; }
			[XmlElement(ElementName = "sum")]
			public string Sum { get; set; }
			[XmlElement(ElementName = "fields")]
			public Fields Fields { get; set; }
			[XmlElement(ElementName = "prv_txn_id")]
			public string Prv_txn_id { get; set; }
			[XmlElement(ElementName = "result")]
			public string Result { get; set; }
			[XmlElement(ElementName = "comment")]
			public string Comment { get; set; }
		}
	}
}