﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class SimplifiedServiceVM
    {
        public int? gate_service_id { get; set; }
        public string name { get; set; }
    }
}
