﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;

namespace DAL.Models
{

    public class CheckHistory
    {
        public int? id { get; set; }

        public int? separate { get; set; }
        public int? serviceid { get; set; }

        public string? name { get; set; }
        public DateTime? start_time { get; set; }
        public DateTime? end_time { get; set; }
        public string? request { get; set; }
        public string? response { get; set; }
        public string? exceptiondata { get; set; }
        public int? resultcodeid { get; set; }
        public int partitionnumber { get; set; }
        public int? userid { get; set; }
        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }

    public class Services
    {
        public int id { get; set; }
        // public int? provider_id { get; set; }
        public int? group_id { get; set; }
        public string? name { get; set; }
        public bool? active { get; set; }
        //  public string? main_id { get; set; }
        // public string? prv_id { get; set; }
        // public string? gate_id { get; set; }
        public int? gate_service_id { get; set; }

        public List<CheckRequestParameters> TestDatas { get; } = new();

    }
}