﻿using System;
using System.IO;
using Microsoft.AspNetCore.Mvc;

namespace DAL.Models
{
    public class LOGGER
    {

        static string loggerMessage;
        public static void LoggerExecute(string message, ControllerBase ctrl)
        {
            string actionName = ctrl.ControllerContext.RouteData.Values["action"].ToString();
            string controllerName = ctrl.ControllerContext.RouteData.Values["controller"].ToString();
            using (FileStream fs = new FileStream("C:\\PSA_Logs\\" + DateTime.Now.ToString("yyyy-MM-dd") + ".log", FileMode.Append, FileAccess.Write, FileShare.ReadWrite))
            using (StreamWriter streamWriter = new StreamWriter(fs))
            {
                streamWriter.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + " " + controllerName + "_" + actionName + "_" + message);
            }
        }
        public static void LoggerStart(string input, ControllerBase contrlr)
        {
            LoggerExecute(input, contrlr);
        }
        public static void LoggerRequest(string txn_or_account, string request, ControllerBase contrlr)
        {
            try
            {
                loggerMessage = txn_or_account + " " + request;
                LoggerExecute(loggerMessage, contrlr);
            }
            catch (Exception)
            {
                return;
            }
        }
        public static void LoggerResponse(string txn_or_account, string result, ControllerBase contrlr, Exception ex = null)
        {
            try
            {
                loggerMessage = txn_or_account + " " + result + ((ex == null) ? "" : "; " + ex.Message + "; " + ex.StackTrace);
                LoggerExecute(loggerMessage, contrlr);
            }
            catch (Exception)
            {
                return;
            }
        }
        public static void LoggerFinish(string input, ControllerBase contrlr)
        {
            LoggerExecute(input, contrlr);
        }
        public static void LoggerReport(string msg, ControllerBase contrlr)
        {
            LoggerExecute(msg, contrlr);
        }
    }
}