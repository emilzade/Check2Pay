﻿namespace DAL.Models
{
    public class Group
    {
        public int id { get; set; }
        public int servicegroupid { get; set; }
        public int? parentservicegroupid { get; set; }
        public string name { get; set; }
        public bool enabled { get; set; }
        public string? comment { get; set; }
        public int? orderid { get; set; }
    }
}
