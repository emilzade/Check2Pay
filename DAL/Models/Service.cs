﻿namespace DAL.Models
{
    public class service
    {
        public int id { get; set; }
        // public int? provider_id { get; set; }
        public int? group_id { get; set; }
        public string? name { get; set; }
        public bool? active { get; set; }
        //  public string? main_id { get; set; }
        // public string? prv_id { get; set; }
        // public string? gate_id { get; set; }
        public int? gate_service_id { get; set; }
        public int? separate { get; set; }
        public int? topservicesid { get; set; }
        public List<CheckRequestHistory> checkRequestHistories { get; set; }
        public List<CheckRequestParameters> TestDatas { get; } = new();
    }
}
