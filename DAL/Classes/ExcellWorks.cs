﻿using Microsoft.AspNetCore.Http;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using DAL.Models;
using LicenseContext = OfficeOpenXml.LicenseContext;

namespace DAL.Classes
{
    public class ExcellWorks
    {


        //string filename = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\check2payreport\\" + string.Format("{0}CheckHistory-{1:yyyy-MM-dd--HH-mm}.xlsx", "", DateTime.Now);
        string filename = "C:\\check2payreport\\" + string.Format("{0}CheckHistory-{1:yyyy-MM-dd--HH-mm}.xlsx", "", DateTime.Now);
        public string CreateAndImport(int? srv_id, string? srv_name, int? part_num, PSA_Context pSA_Context)
        {
            try
            {
                string dir = @"C:\check2payreport";
                // If directory does not exist, create it
                if (!Directory.Exists(dir))
                {
                    Directory.CreateDirectory(dir);
                }


                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;

                if (File.Exists(filename))
                {
                    File.Delete(filename);
                }

                var package = new ExcelPackage(filename);
                var workSheet = package.Workbook.Worksheets.Add("Sheet1");

                DbWorks dp = new DbWorks(pSA_Context);
                List<CheckHistory> list = dp.GetUnsuccesHistory(srv_id, srv_name, part_num, Convert.ToDateTime("2022-01-01"), Convert.ToDateTime("2025-01-01"));
                int row = 2;

                foreach (CheckHistory item in list)
                {
                    addDataCentered(workSheet, row, 1, item.serviceid.ToString());
                    addDataCentered(workSheet, row, 2, item.name.ToString());
                    addDataCentered(workSheet, row, 3, item.start_time.ToString());
                    addDataCentered(workSheet, row, 4, item.userid.ToString());
                    addDatasade(workSheet, row, 5, item.request.ToString());
                    addDatasade(workSheet, row, 6, item.response.ToString());
                    addDataCentered(workSheet, row, 7, item.exceptiondata.ToString());
                    addDataCentered(workSheet, row, 8, item.resultcodeid.ToString());
                    addDataCentered(workSheet, row, 9, item.partitionnumber.ToString());
                    addDataCentered(workSheet, row, 10, item.end_time.ToString());
                    addDataCentered(workSheet, row, 11, item.separate.ToString());
                    row++;
                }
                addHeaders(workSheet);

                workSheet.Cells[1, 1].EntireColumn.Width = 21;
                workSheet.Cells[1, 2].EntireColumn.Width = 21;
                workSheet.Cells[1, 3].EntireColumn.Width = 21;
                workSheet.Cells[1, 4].EntireColumn.Width = 21;
                workSheet.Cells[1, 5].EntireColumn.Width = 21;
                workSheet.Cells[1, 6].EntireColumn.Width = 21;
                workSheet.Cells[1, 7].EntireColumn.Width = 21;
                workSheet.Cells[1, 8].EntireColumn.Width = 21;
                workSheet.Cells[1, 9].EntireColumn.Width = 21;
                workSheet.Cells[1, 10].EntireColumn.Width = 21;
                workSheet.Cells[1, 11].EntireColumn.Width = 21;


                FileInfo excelFile = new FileInfo(filename);
                package.SaveAs(filename);


                package.Dispose();
                return filename;
            }
            catch (Exception ex)
            {

                return ex.Message;
            }
        }
        private void addDataCentered(ExcelWorksheet worksheet, int row, int col, string? data)
        {
            worksheet.Cells[row, col].Value = data;
            worksheet.Cells[row, col].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        }
        private void addDatasade(ExcelWorksheet worksheet, int row, int col, string data)
        {
            worksheet.Cells[row, col].Value = data;
        }
        private void addHeaders(ExcelWorksheet workSheet)
        {
            addDataCentered(workSheet, 1, 1, "Service ID");
            workSheet.Cells[1, 1].Style.Font.Bold = true;
            workSheet.Cells[1, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            addDataCentered(workSheet, 1, 2, "Service Name");
            workSheet.Cells[1, 2].Style.Font.Bold = true;
            workSheet.Cells[1, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            addDataCentered(workSheet, 1, 3, "Testing Date");
            workSheet.Cells[1, 3].Style.Font.Bold = true;
            workSheet.Cells[1, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            addDataCentered(workSheet, 1, 4, "User ID");
            workSheet.Cells[1, 4].Style.Font.Bold = true;
            workSheet.Cells[1, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            addDataCentered(workSheet, 1, 5, "Request Body");
            workSheet.Cells[1, 5].Style.Font.Bold = true;
            workSheet.Cells[1, 5].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            addDataCentered(workSheet, 1, 6, "Response Body");
            workSheet.Cells[1, 6].Style.Font.Bold = true;
            workSheet.Cells[1, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            addDataCentered(workSheet, 1, 7, "Exception Data");
            workSheet.Cells[1, 7].Style.Font.Bold = true;
            workSheet.Cells[1, 7].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            addDataCentered(workSheet, 1, 8, "Result Code");
            workSheet.Cells[1, 8].Style.Font.Bold = true;
            workSheet.Cells[1, 8].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            addDataCentered(workSheet, 1, 9, "Partition Number");
            workSheet.Cells[1, 9].Style.Font.Bold = true;
            workSheet.Cells[1, 9].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            addDataCentered(workSheet, 1, 10, "End Time");
            workSheet.Cells[1, 10].Style.Font.Bold = true;
            workSheet.Cells[1, 10].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            addDataCentered(workSheet, 1, 11, "Separate");
            workSheet.Cells[1, 11].Style.Font.Bold = true;
            workSheet.Cells[1, 11].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        }
    }
}


