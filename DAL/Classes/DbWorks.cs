﻿using Dapper;
using Microsoft.EntityFrameworkCore;
using DAL.Models;
using System.Data.Common;
using System.Linq;
using Microsoft.EntityFrameworkCore.Internal;

namespace DAL.Classes
{
    public class DbWorks
    {
        private readonly PSA_Context _pSA_Context;
        public DbWorks(PSA_Context context)
        {
            _pSA_Context = context;
        }
        public List<CheckHistory> GetHistory()
        {
            List<CheckHistory> list = new List<CheckHistory>();
            using (var con = _pSA_Context.Database.GetDbConnection())
            {
                string sql;
                sql = @"SELECT s.separate , c.id,serviceid,s.name ,  userid, request, response, exceptiondata, resultcodeid, partitionnumber, start_time, end_time
                        FROM check2pay.checkrequesthistory c
                        inner join check2pay.service s on c.serviceid=s.gate_service_id order by c.start_time desc";
                list = con.Query<CheckHistory>(sql).ToList();
            }
            return list;
        }
        public List<CheckHistory> GetHistory(int serviceId)
        {
            List<CheckHistory> list = new List<CheckHistory>();
            using (var con = _pSA_Context.Database.GetDbConnection())
            {
                string sql;
                DynamicParameters parameters = new DynamicParameters();
                sql = @"SELECT s.separate ,c.id,serviceid,s.name , userid, request, response, exceptiondata, resultcodeid, partitionnumber, start_time, end_time
                        FROM check2pay.checkrequesthistory c
                        inner join check2pay.service s on c.serviceid=s.gate_service_id where serviceid = @s_id order by c.start_time desc ;";
                parameters.Add("s_id", serviceId);
                list = con.Query<CheckHistory>(sql, parameters).ToList();
            }
            return list;
        }
        public List<CheckRequestHistory> GetHistory1(int? serviceId, string? srv_name, int? partition_num, int? status, DateTime from_date, DateTime to_date)
        {
            List<CheckRequestHistory> checkHistories = new List<CheckRequestHistory>();

            checkHistories = _pSA_Context.checkrequesthistory
                .Where(x => serviceId == null || x.serviceid == serviceId)
                .Where(x => x.partitionnumber == partition_num || partition_num == null)
                .Include(x => x.service.checkRequestHistories.Where(x => x.service.name.Contains(srv_name)))
                .ToList();
            return checkHistories;
        }
        public List<CheckHistory> GetHistory(int? serviceId, string? srv_name, int? partition_num, int? status, DateTime from_date, DateTime to_date)
        {
            List<CheckHistory> list = new List<CheckHistory>();
            //using (var con = _pSA_Context.Database.GetDbConnection())
            //{
            var con = _pSA_Context.Database.GetDbConnection();
            string sql;
            DynamicParameters parameters = new DynamicParameters();
            if (srv_name is null)
            {
                sql = @"select  s.separate, c.id, serviceid, s.name, userid, request, response, exceptiondata, resultcodeid, partitionnumber, start_time, end_time
                      from check2pay.checkrequesthistory as c
                      inner join check2pay.service as s on c.serviceid = s.gate_service_id
                      where c.resultcodeid= coalesce(@status,c.resultcodeid)
                      and c.serviceid = coalesce(@s_id,c.serviceid) 
                      and c.start_time between @startdate and @enddate 
                      and partitionnumber = coalesce(@prt_num,partitionnumber) order by c.start_time desc;";

                parameters.Add("prt_num", partition_num);
                parameters.Add("s_id", serviceId);
                parameters.Add("startdate", from_date);
                parameters.Add("enddate", to_date);
                parameters.Add("status", status);
            }
            else
            {
                sql = @"select  s.separate ,c.id,serviceid,s.name , userid, request, response, exceptiondata, resultcodeid, partitionnumber, start_time, end_time from check2pay.checkrequesthistory as c 
                           inner join check2pay.service as s on c.serviceid = s.gate_service_id
                             where c.resultcodeid=coalesce(@status,c.resultcodeid)and c.serviceid = coalesce(@s_id,c.serviceid) and s.name ILIKE '%" + srv_name + "%' and c.start_time between @startdate and @enddate and " +
                           "partitionnumber = coalesce(@prt_num,partitionnumber) order by c.start_time desc";
                parameters.Add("s_id", serviceId);
                parameters.Add("srv_nam", srv_name);
                parameters.Add("prt_num", partition_num);
                parameters.Add("startdate", from_date);
                parameters.Add("enddate", to_date);
                parameters.Add("status", status);

            }
            list = con.Query<CheckHistory>(sql, parameters).ToList();
            return list;
        }
        public List<Group> GetGroups()
        {
            string sql;
            List<Group> grps = new List<Group>();
            var con = _pSA_Context.Database.GetDbConnection();
            sql = "select * from check2pay.groupservices";
            grps = con.Query<Group>(sql).ToList();
            return grps;
        }
        public List<service> GetTopServices(int limit)
        {
            string sql;
            var con = _pSA_Context.Database.GetDbConnection();
            DynamicParameters parameters = new DynamicParameters();
            sql = "select * from check2pay.service where topservicesid <= @limit order by topservicesid";
            parameters.Add("@limit", limit);
            var list = con.Query<service>(sql, parameters).ToList();
            return list;
        }
        public List<CheckHistory> GetUnsuccesHistory(int? serviceId, string? srv_name, int? partition_num, DateTime from_date, DateTime to_date)
        {
            List<CheckHistory> list = new List<CheckHistory>();
            //using (var con = _pSA_Context.Database.GetDbConnection())
            //{
            var con = _pSA_Context.Database.GetDbConnection();
            string sql;
            DynamicParameters parameters = new DynamicParameters();
            if (srv_name is null)
            {
                sql = @"select gs.separate ,gs.name, ch.* from check2pay.checkrequesthistory as ch 
                           left join check2pay.service as gs on ch.serviceid = gs.gate_service_id
                           where ch.resultcodeid=1 and gs.separate is null   and ch.serviceid = coalesce(@s_id,ch.serviceid) and ch.start_time between @startdate and @enddate and partitionnumber = coalesce(@prt_num,partitionnumber) order by ch.start_time desc;";

                parameters.Add("prt_num", partition_num);
                parameters.Add("s_id", serviceId);
                parameters.Add("startdate", from_date);
                parameters.Add("enddate", to_date);
            }
            else
            {
                sql = @"select gs.separate ,gs.name, ch.* from check2pay.checkrequesthistory as ch 
                           left join check2pay.service as gs on ch.serviceid = gs.gate_service_id
                             where ch.resultcodeid=1 and gs.separate is null ch.serviceid = coalesce(@s_id,ch.serviceid) and gs.name ILIKE '%" + srv_name + "%' and ch.start_time between @startdate and @enddate and " +
                           "partitionnumber = coalesce(@prt_num,partitionnumber) order by ch.start_time desc";
                parameters.Add("s_id", serviceId);
                parameters.Add("srv_nam", srv_name);
                parameters.Add("prt_num", partition_num);
                parameters.Add("startdate", from_date);
                parameters.Add("enddate", to_date);
            }
            list = con.Query<CheckHistory>(sql, parameters).ToList();
            return list;
        }
        public List<CheckHistory> GetHistory(DateTime from_date, DateTime? to_date)
        {
            List<CheckHistory> list = new List<CheckHistory>();
            using (var con = _pSA_Context.Database.GetDbConnection())
            {
                string sql;
                DynamicParameters parameters = new DynamicParameters();
                if (to_date is null)
                {
                    sql = @"SELECT s.separate ,c.id,serviceid,s.name ,  userid, request, response, exceptiondata, resultcodeid, partitionnumber, start_time, end_time
                        FROM check2pay.checkrequesthistory c
                        inner join check2pay.service s on c.serviceid=s.gate_service_id 
                                     where c.start_time  between @from_date and now();";
                    parameters.Add("from_date", from_date);
                }
                else
                {
                    sql = @"SELECT s.separate ,c.id,serviceid,s.name ,  userid, request, response, exceptiondata, resultcodeid, partitionnumber, start_time, end_time
                        FROM check2pay.checkrequesthistory c
                        inner join check2pay.service s on c.serviceid=s.gate_service_id 
                                     where start_time  between @from_date and @to_date;";
                    parameters.Add("from_date", from_date);
                    parameters.Add("to_date", to_date);
                }
                list = con.Query<CheckHistory>(sql, parameters).ToList();
            }
            return list;
        }
        public async Task<List<CheckRequestParameters>> GetParametersByServiceId(int? prv_id)
        {
            List<CheckRequestParameters> lst = new List<CheckRequestParameters>();
            using (var con = _pSA_Context.Database.GetDbConnection())
            {
                string sql = "SELECT * FROM check2pay.checkrequestparameters where serviceid = @prv_id";
                DynamicParameters par = new DynamicParameters();
                par.Add("prv_id", prv_id);
                lst = con.Query<CheckRequestParameters>(sql, par).ToList();
            }
            return lst;
        }
        public async Task<bool> UpdateParametersBySrvID(List<CheckRequestParameters> prms_list)
        {
            int result = 0;

            using (var con = _pSA_Context.Database.GetDbConnection())
            {
                foreach (CheckRequestParameters prms in prms_list)
                {
                    string sql = "update check2pay.checkrequestparameters set name = @name, value = @value, active = @active where serviceid = @srv_id and id = @id";
                    DynamicParameters par = new DynamicParameters();
                    par.Add("name", prms.name);
                    par.Add("value", prms.value);
                    par.Add("active", prms.active);
                    par.Add("srv_id", prms.serviceid);
                    par.Add("id", prms.id);
                    result = await con.ExecuteAsync(sql, par);
                }
            }
            return intToBool(result);
        }
        public async Task<bool> AddParameterToSrv(CheckRequestParameters prm)
        {
            int result = 0;
            using (DbConnection con = _pSA_Context.Database.GetDbConnection())
            {
                string sql = "insert into check2pay.checkrequestparameters (serviceid, name, value, active) values (@srv_id, @name, @value, @active)";
                DynamicParameters par = new DynamicParameters();
                par.Add("name", prm.name);
                par.Add("value", prm.value);
                par.Add("active", prm.active);
                par.Add("srv_id", prm.serviceid);
                result = await con.ExecuteAsync(sql, par);
            }
            return intToBool(result);
        }
        public List<service> FilterService(string id, int groupId)
        {
            List<service> services = new List<service>();
            var con = _pSA_Context.Database.GetDbConnection();
            string sql;
            DynamicParameters parameters = new DynamicParameters();
            if (groupId == 0)
            {

                sql = "select * from check2pay.service s  where cast(gate_service_id as text) Like '%" + id + "%'";
            }
            else
            {
                sql = "select * from check2pay.service s  where cast(gate_service_id as text) Like '%" + id + "%' and group_id = " + groupId;
            }
            services = con.Query<service>(sql, parameters).ToList();
            return services;
        }
        private bool intToBool(int val)
        {
            if (val == 0)
            { return false; }
            else
            { return true; }
        }
    }
}