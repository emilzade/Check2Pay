﻿using DAL.Models;

namespace DAL.ViewModels
{
    public class ServiceVM
    {
        public int TotalCount { get; set; }
        public IEnumerable<service> Services { get; set; }
    }
}
